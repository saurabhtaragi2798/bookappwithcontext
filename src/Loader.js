import React ,{Component} from "react";

class Loader extends Component{
  render(){
    return(
      <div className="Loader-center">
        <i className="fas fa-spinner fa-pulse fa-10x"></i>
      </div>
    )
  }
}

export default Loader;
