import React,{Component,Suspense,lazy} from "react";
import ReactDynamicImport from "react-dynamic-import";
import Dropdown from "./Dropdown";
import axios from "axios";
import Loader from "./Loader";
import BookContext,{ withBookData } from "./Context";
import BookItem from "./BookItem";
class BookList extends Component {
  static contextType = BookContext
  constructor(props){
    super(props);
    this.state = {
      isLoad:true
    }

    this.bookFilter = [
      {
        id: 4,
        name: 'All'
      },
      {
        id: 1,
        name: 'English'
      },
      {
        id: 2,
        name: 'German'
      },
      {
        id: 3,
        name: 'Russian'
      }
    ]

    this.typeFilter = [
      {
        id: 4,
        name: 'All'
      },
      {
        id: 1,
        name: 'Latest'
      },
      {
        id: 2,
        name: 'Mid'
      },
      {
        id: 3,
        name: 'Old'
      }
    ]
  }

  componentDidMount()
  {
    this.getData();
  }

  getData = async () => {
    try{
      let result = await axios.get(" https://5125f233.ngrok.io/getBookData");
      this.props.updateData(result.data);
      this.setState({isLoad:false});
    }
    catch(err){
      console.log(err);
    }
  }

  filterByLanguage = async (id) => {
    try{
      let result = await axios.get(`https://5125f233.ngrok.io/filterByLanguage/${id}`);
      await this.props.updateData(result.data);
      this.setState({isLoad:false});
    }
    catch(err){
      console.log(err);
    }
  }

  filterByType = async (id) => {
    try{
      let result = await axios.get(`https://5125f233.ngrok.io/filterByType/${id}`);
      await this.props.updateData(result.data);
      this.setState({isLoad:false});
    }
    catch(err){
      console.log(err)
    }
  }

  onChangeFilter = (dataType, id) => {
    this.setState({isLoad:true})
    if(dataType === "typeFilter"){
      this.filterByType(id);
    }
    else if(dataType === "bookFilter"){
      this.filterByLanguage(id);
    }
  }

  render() {
    return(
      <React.Fragment>
        <div className="con">
          <div className="div1">
              Filter By Type
            <Dropdown data={this.typeFilter} dataType="typeFilter" onChangeFilter={this.onChangeFilter}/>
          </div>
          <div className="div2">
            Filter By Language
            <Dropdown data={this.bookFilter} dataType="bookFilter" onChangeFilter={this.onChangeFilter}/>
          </div>
        </div>
        {(this.state.isLoad) && <Loader/>}
        {!(this.state.isLoad) && <BookItem/>}
      </React.Fragment>
    );
  }
}
export default withBookData(BookList);
