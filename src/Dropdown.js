import React,{Component} from "react";

class Dropdown extends Component {

  filterData = (dataType, e) => {
    this.props.onChangeFilter(dataType,e.target.selectedOptions[0].value);
  }

  render(){
    let dataType = this.props.dataType;
    return (
      <div>
        <select onChange={(e) => this.filterData(dataType, e)} className="dropdown">
          {
            (this.props.data) && this.props.data.map((item,index) => {
              return <option key={index} value={item.id}>{item.name}</option>
            })
          }
          </select>
        </div>
        );
    }
}

export default Dropdown;
