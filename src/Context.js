import React,{Component} from "react";

const BookContext = React.createContext({
  bookData :[],
  updateData : () =>{}
});

export default BookContext;

export const withBookData = (OriginalComponent) => {
  class NewComponent extends Component {
    render(){
      return(
        <BookContext.Consumer>
          {
            ({bookData,updateData}) => {
            return <OriginalComponent  {...this.props} data={bookData} updateData={updateData}/>
            }
          }
        </BookContext.Consumer>
      );
    }
  }
  return NewComponent;
}
