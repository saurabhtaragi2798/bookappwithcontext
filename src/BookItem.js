import React,{Component} from "react";
import { Link } from "react-router-dom";
import BookContext,{ withBookData } from "./Context";
  class BookItem extends Component {
    static contextType = BookContext;
    itemType = (item) =>{
        switch (item) {
          case 1:   return "Latest Book";
          case 2: return "Mid Book";
          case 3:  return "Old Book";
          default: return "";
        }
    }
    render(){
    return(
      <div>
        { this.props.data && this.props.data.map((item, index) => {
            return(
              <div className="row5" key={index}>
                <div className="row3" align="left">
                  <img src={item.imagelink} width="100px" height="100px" alt=""/>
                </div>
                <div className="row4" align="right" >
                  <p><b>Book Name</b>:  {item.title}</p>
                  <p><b>Author Name</b>:  {item.author}</p>
                  <p><b>Book Language</b>:  {item.language}</p>
                  <p><b>Country</b>:  {item.country}</p>
                  <p><b>Type</b>:{this.itemType(item.type)}</p>
                  <Link to={`/books/${item.id}`} className="btn btn-info">Book Detail</Link>
                </div>
            </div>
            )
          })
        }
      </div>
  );
  }
}
export default withBookData(BookItem);
