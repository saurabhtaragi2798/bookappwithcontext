import React,{Component} from "react";
import logo from "./logo.jpeg";

class Header extends Component {
  render(){
    return(
      <div>
      <ul className="nav">
    <li className="nav-item">
    <img src={logo} width="100px" height="100px" alt=""/>
      </li>
  </ul>
      </div>
    );
  }
}
export default Header;
