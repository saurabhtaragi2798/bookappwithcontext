import React, {Component} from "react";
import axios from 'axios';
import Loader from "./Loader"
import BookContext,{ withBookData } from "./Context";
class Bookdetail extends Component {
  static contextType = BookContext;
  constructor(props){
    super(props);
    this.state = {
      isLoad:true
    };
  }
  componentDidMount(){
    let bookId = this.props.match.params.id;
    this.getBookDetail(bookId);
  }

  getBookDetail = async (id) =>{
    try{
      let result = await axios.get(`https://5125f233.ngrok.io/getBookDetail/${id}`);
      this.props.updateData(result.data);
      this.setState({isLoad:false});
    }
    catch(err){
      console.log(err);
    }
  }
  getBookDetailView = (item,index) => {
    return(
      <div className="book-detail" key={index}>
        <div className="image"><img src={item.imagelink} width="300px" height="300px" alt=""/></div>
        <p><b>Book Name</b>:  {item.title}</p>
        <p><b>Author Name</b>:  {item.author}</p>
        <p><b>Book Language</b>:  {item.language}</p>
        <p><b>Country</b>:  {item.country}</p>
        <p><b>Country</b>:  {item.description}<a href={item.link}>Read More</a></p>
      </div>
    )
  }
  render(){
    if(this.state.isLoad) return (<Loader/>)
      return(
        <div>
          {
            this.props.data.map((item, index) =>{
            return this.getBookDetailView(item,index);
            })
          }
      </div>
    )
  }
}
  export default withBookData(Bookdetail);
