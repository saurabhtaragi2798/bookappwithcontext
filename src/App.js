import React, { Component } from 'react';
import './App.css';
import Header from "./Header"
import BookList from "./BookList"
import Bookdetail from "./Bookdetail"
import BookContext,{ withBookData }  from "./Context";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      data:[],
    };
  }
  updateData = (data) =>{
    let bookData = Object.assign([],data);
    this.setState({data:bookData})
  }
  setId = (id) =>{
    this.setState({id:id})
  }
  render(){
    return(
    <div>
      <BookContext.Provider value={{bookData:this.state.data,updateData:this.updateData}}>
        <Header/>
        <Router>
          <Switch>
            <Route exact path={["/","/books"]} component={BookList}/>
            <Route path="/books/:id" component={Bookdetail}/>
          </Switch>
        </Router>
        </BookContext.Provider>
    </div>
    );
  }
}

export default withBookData(App);
